﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace ScalarmAuthentication
{
    class Program
    {
        static void Main(string[] args)
        {
            Certificate cert = new Certificate();
            cert.Name = "plgbachniak.p12";
            cert.Path = "H:\\_CYFRONET\\";
            cert.Password = Encoding.UTF8.GetBytes(ReadPassword("Enter PL-Grid Certificate password:"));
            bool exist = cert.isExist();

            Proxy proxy = new Proxy();
            proxy.Name = "plgbachniak.pem";
            proxy.Path = "H:\\_CYFRONET\\";
            proxy.ExpirationTime = DateTime.Now + new TimeSpan(10, 2, 0, 30, 0);

            bool result = ProxyManager.generateProxy(cert, ref proxy);
            
            /*Certificate abc = new Certificate();
            abc.Name = "plglrosol.p12";
            abc.Path = "G:\\globus-lite";

            Console.WriteLine(abc.isExist());
            Console.ReadLine();*/
        }

        public static string ReadString(string prompt)
        {
            Console.Write(prompt + " ");
            return Console.ReadLine();
        }

        public static string ReadPassword(string prompt)
        {
            string pass = "";
            Console.Write(prompt + " ");
            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);

            Console.WriteLine("\n");

            return pass;
        }
    }
}