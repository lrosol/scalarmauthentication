﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScalarmAuthentication
{
    [TestClass]
    public class CertificateTest
    {
        private Certificate testProxyObj;
        private const string testCertificateFilename = "cert.p12";
        public CertificateTest()
        {
            this.testProxyObj = new Certificate();
        }

        [TestMethod]
        public void ExistCertificatie()
        {
            Certificate testProxy = new Certificate();
            testProxy.Name = testCertificateFilename;
            Assert.AreEqual(true, testProxy.isExist());
        }

        [TestMethod]
        public void PasswordAccessor()
        {
            string pass = "abcd";
            this.testProxyObj.Password = Encoding.UTF8.GetBytes(pass);
            string decrypted = Encoding.UTF8.GetString(this.testProxyObj.Password);
            Assert.AreEqual(pass, decrypted);
        }
    }

    [TestClass]
    public class ProxyManagerTest
    {
        [TestMethod]
        public void GenerateProxy()
        {
            Certificate cert = new Certificate();
            cert.Name = "cert.p12";
            cert.Path = Directory.GetCurrentDirectory();
            cert.Password = Encoding.UTF8.GetBytes("q2NtFtwy9");
            
            Proxy proxy = new Proxy();
            proxy.Name = "dest";
            proxy.Path = "G:\\globus-lite\\";
            proxy.ExpirationTime = DateTime.Now + new TimeSpan(0, 2, 0, 30, 0);
            Assert.AreEqual(false, proxy.isExist());

            bool result = ProxyManager.generateProxy(cert, ref proxy);
            Assert.AreEqual(true, result);
            Assert.AreEqual(true, proxy.isExist());
        }
    }
}
