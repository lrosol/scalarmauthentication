@ECHO OFF
SETLOCAL
:: Read number of passed arguments
SET argc=0
FOR %%x in (%*) DO SET /A argc+=1
SET expargs=4
:: Check arguments
IF %argc% NEQ %expargs% SET errmsg=%argc% arguments passed. Expected %expargs%: [basecert] [destcert] [expdate] [password] && GOTO ERROR
:: Set local variables
SET basecert=%1
SET destcert=%2
SET expdate=%3
SET password=%4
:: Used by grid-proxy-init
SET X509_USER_PROXY=%destcert%
:: Run external executable
ECHO %password%|grid-proxy\grid-proxy-init -cert %basecert% -key %basecert% -valid %expdate% -pwstdin -rfc
:: Verify proxy geneartion process
IF %errorlevel% NEQ 0 SET errmsg=Proxy generation failed. && GOTO ERROR
GOTO END

:ERROR
ECHO Error: %errmsg%
GOTO END

:END
ENDLOCAL
EXIT %errorlevel%