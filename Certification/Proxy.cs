﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScalarmAuthentication
{
    public class Proxy : FileHandler
    {
        private DateTime expirationTime;
        public DateTime ExpirationTime
        {
            get
            {
                return this.expirationTime;
            }
            set
            {
                this.expirationTime = value;
            }
        }
    }
}
