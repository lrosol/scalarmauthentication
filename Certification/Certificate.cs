﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace ScalarmAuthentication
{
    public class Certificate : FileHandler
    {
        public Certificate():base()
        {
            this.Password = new byte[0];
        }

        private byte[] password;
        private byte[] entropy;
        public byte[] Password
        {
            get
            {
                return ProtectedData.Unprotect(this.password, this.entropy, DataProtectionScope.CurrentUser);
            }
            set
            {
                // Generate additional entropy (will be used as the Initialization vector)
                this.entropy = new byte[20];
                using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                {
                    rng.GetBytes(entropy);
                }

                this.password = ProtectedData.Protect(value, this.entropy,
                    DataProtectionScope.CurrentUser);
            }
        }
    }
}
