﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ScalarmAuthentication
{
    public class ProxyManager
    {
        public static bool generateProxy(Certificate certificate, ref Proxy destination)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "proxy-init-wrapper.bat";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            TimeSpan validTime = destination.ExpirationTime - DateTime.Now;
            string validationTime = validTime.Hours + ":" + validTime.Minutes;
            Console.WriteLine(validationTime);
            startInfo.Arguments = certificate.FullPath + " " + destination.FullPath + " " + validationTime + " " + Encoding.UTF8.GetString(certificate.Password);

            

            try
            {
                

                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                    return exeProcess.ExitCode == 0 ? true : false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
