﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ScalarmAuthentication
{
    public class FileHandler : IFileHandler
    {
        public FileHandler()
        {
            this.Name = "";
            this.Path = "";
        }

        private string proxyName;
        public string Name
        {
            get
            {
                return this.proxyName;
            }
            set
            {
                this.proxyName = value;
            }
        }
        private string proxyPath;
        public string Path
        {
            get
            {
                return this.proxyPath;
            }
            set
            {
                if (value.Equals("") || value.EndsWith("\\")) {
                    this.proxyPath = value;
                } else
                    this.proxyPath = value + "\\";
            }
        }

        public string FullPath
        {
            get
            {
                return this.Path + this.Name;
            }
        }

        public Boolean isExist()
        {
            string filename = this.proxyPath + this.proxyName;
            return File.Exists(filename);
        }
    }
}
