﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace ScalarmAuthentication
{
    interface IFileHandler
    {
        string Name
        {
            get;
            set;
        }

        string Path
        {
            get;
            set;
        }

        string FullPath
        {
            get;
        }

        Boolean isExist();
    }
}
